from django.utils.timezone import now
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from api import models, serializers, queries


class SupplyViewSet(viewsets.ModelViewSet):
    queryset = models.Supply.objects.all()
    serializer_class = serializers.SupplySerializer

    @action(detail=False, methods=['GET'])
    def sum_supply(self, request):
        year = int(request.query_params.get('year', now().year))
        month = int(request.query_params.get('month', now().month))

        sum_supply = queries.sum_quantity_liters_supply_month(month, year)
        return Response(sum_supply)

    @action(detail=False, methods=['GET'])
    def diff_km_supply(self, request):
        year = int(request.query_params.get('year', now().year))
        month = int(request.query_params.get('month', now().month))

        diff_supply = queries.calculate_km_supply_month(month, year)
        return Response(
            {
                'quantity_km': diff_supply
            }
        )

    @action(detail=False, methods=['GET'])
    def count_supply(self, request):
        year = int(request.query_params.get('year', now().year))
        month = int(request.query_params.get('month', now().month))

        supply = queries.count_supply_month(month, year)
        return Response(
            {
                'quantity_supply': supply
            }
        )

    @action(detail=False, methods=['GET'])
    def calculate_km_liters(self, request):
        year = int(request.query_params.get('year', now().year))
        month = int(request.query_params.get('month', now().month))

        performance = queries.calculate_km_liters(month, year)
        return Response(
            {
                'km_liters': performance
            }
        )
